<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'User\IndexController@index')->name('index');

// Auth
Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function() {
    Route::get('/user/login', 'UserController@showLoginForm')->name('show_user_login_form');
    Route::post('/user/login', 'UserController@login')->name('user_login');;
    Route::get('/user/logout', 'UserController@logout')->name('user_logout');
    Route::get('/user/sns/{snsProvider}', 'UserController@snsLogin')->name('sns_login');
    Route::get('/user/{snsProvider}/callback', 'UserController@snsLoginCallback');
    Route::get('/user/register','UserController@showRegisterForm')->name('show_user_register_form');
    Route::post('/user/register', 'UserController@registerUser')->name('register_user');
    Route::get('/user/verify-email/{token}', 'UserController@verifyEmail')->name('verify_user_email');
    Route::get('/user/forget-password','UserController@showForgetPasswordForm')->name('show_user_forget_password_form');
    Route::post('/user/forget-password', 'UserController@createResetPasswordToken')->name('create_user_reset_password_token');
    Route::get('/user/reset-password/{token}', 'UserController@showResetPasswordForm')->name('show_user_reset_password_form');
    Route::post('/user/reset-password/{token}', 'UserController@resetPassword')->name('reset_user_password');
    Route::get('/user/password', 'UserController@password')->name('user_password');
    Route::patch('/user/change-password', 'UserController@changePassword')->name('change_user_password');

    Route::get('/admin/login', 'AdminController@showLoginForm')->name('show_admin_login_form');
    Route::post('/admin/login', 'AdminController@login')->name('admin_login');
    Route::get('/admin/logout', 'AdminController@logout')->name('admin_logout');
    Route::patch('/admin/change-password', 'AdminController@changePassword')->name('change_admin_password');
    Route::get('/admin/forget-password','AdminController@showForgetPasswordForm')->name('show_admin_forget_password_form');
    Route::post('/admin/forget-password', 'AdminController@createResetPasswordToken')->name('create_admin_reset_password_token');
    Route::get('/admin/reset-password/{token}', 'AdminController@showResetPasswordForm')->name('show_admin_reset_password_form');
    Route::post('/admin/reset-password/{token}', 'AdminController@resetPassword')->name('reset_admin_password');
});

Route::group(['namespace' => 'User', 'as' => 'user.', 'middleware' => ['auth:user', 'localization:user']], function() {

    // Profile
    Route::get('/profile', 'ProfileController@profile')->name('profile');
    Route::patch('/profile', 'ProfileController@updateProfile')->name('update_profile');
    Route::patch('/password', 'ProfileController@updatePassword')->name('update_password');

    Route::get('/dashboard', 'IndexController@dashboard')->name('dashboard');

});

// Admin
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => ['auth:admin', 'localization:admin']], function() {

    Route::get('/', 'IndexController@index')->name('index');

    // Profile
    Route::get('/profile', 'ProfileController@profile')->name('profile');
    Route::patch('/profile', 'ProfileController@updateProfile')->name('update_profile');
    Route::patch('/password', 'ProfileController@updatePassword')->name('update_password');

    //User
    Route::get('/users', 'UserController@index')->name('users_index');
    Route::get('/users/composer', 'UserController@composer')->name('ajax_load_user_composer');
//    Route::get('/users/export', 'UserController@export')->name('users_export');
//    Route::post('users', 'UserController@store')->name('ajax_create_user');
    Route::patch('/users/{user}', 'UserController@update')->name('ajax_update_user');
//    Route::patch('users', 'UserController@batchUpdate')->name('ajax_update_users');
    Route::delete('users/{user}', 'UserController@delete')->name('ajax_delete_user');
//    Route::post('users/batch-delete', 'UserController@batchDelete')->name('ajax_delete_users');


    // Announcement
	//    Route::get('/announcements', 'AnnouncementController@index')->name('announcement_index');
	//    Route::get('/announcements/composer', 'AnnouncementController@composer')->name('ajax_load_announcement_composer');
	//    Route::post('announcements', 'AnnouncementController@store')->name('ajax_create_announcement');
	//    Route::patch('/announcements/{announcement}', 'AnnouncementController@update')->name('ajax_update_announcement');
	//    Route::delete('announcements/{announcement}', 'AnnouncementController@delete')->name('ajax_delete_announcement');
});

// System admin area
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => ['auth:admin', 'authorize.system_admin', 'localization:admin']], function() {

    // Role
    Route::get('/roles', 'RoleController@index')->name('roles_index');
    Route::get('/roles/composer', 'RoleController@composer')->name('ajax_load_role_composer');
    Route::post('roles', 'RoleController@store')->name('ajax_create_role');
    Route::patch('roles/{role}', 'RoleController@update')->name('ajax_update_role');
    Route::delete('roles/{role}', 'RoleController@delete')->name('ajax_delete_role');

     // Admin
     Route::get('/admins', 'AdminController@index')->name('admins_index');
     Route::get('/admins/composer', 'AdminController@composer')->name('ajax_load_admin_composer');
     Route::post('admins', 'AdminController@store')->name('ajax_create_admin');
     Route::patch('admins/{admin}', 'AdminController@update')->name('ajax_update_admin');
     Route::delete('admins/{admin}', 'AdminController@delete')->name('ajax_delete_admin');
});

