<?php

namespace App\Services;

use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleService
{

    /**
     * @param $params
     * @return mixed
     */
    public function searchRole($params)
    {
        $query =  Role::with('permissions')->withCount('admins', 'users');

        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['name'])) {
            $query->where('name', $params['name']);
        }
        if(!empty($params['guard'])) {
            $query->where('guard', $params['guard']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }

        return $query->paginate(config('app.pagination'));
    }
    /**
     * @param $attributes
     * @return mixed
     */
    public function createRole(array $attributes)
    {
        return DB::transaction(function () use ($attributes) {
            $role = new Role($attributes);
            $role->save();
            $role->permissions()->sync($attributes['permission_ids']);

            return $role->load('permissions');
        });
    }

    /**
     * @param Role $role
     * @param array $attributes
     * @return mixed
     */
    public function updateRole(Role $role, array $attributes)
    {
        return DB::transaction(function () use ($attributes, $role) {
            $role->update($attributes);
            $role->permissions()->sync($attributes['permission_ids']);

            return $role->load('permissions');
        });
    }

    /**
     * @param  Role  $role
     * @return mixed
     */
    public function deleteRole(Role $role)
    {
        return DB::transaction(function () use ($role) {
            $role->permissions()->sync([]);

            return $role->delete();
        });
    }

}
