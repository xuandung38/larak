<?php

namespace App\Services;

//use App\Enums\UserSortTypes;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserService
{

    /**
     * @param $params
     * @return mixed
     */
    public function searchUser($params)
    {
        $query =  User::with('agency')->orderBy('id', 'DESC');

        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['email'])) {
            $query->where('email', $params['email']);
        }
        if(!empty($params['phone'])) {
            $query->where('phone', $params['phone']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('email', 'like', '%' . $params['search'] . '%')
                    ->orWhere('phone', 'like', '%' . $params['search'] . '%')
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }

//        if(!empty($params['sort_type'])) {
//            switch ($params['sort_type']) {
//                case UserSortTypes::ID_ASC:
//                    $query->orderBy('id', 'asc');
//                    break;
//                case UserSortTypes::ID_DESC:
//                    $query->orderBy('id', 'desc');
//                    break;
//                case UserSortTypes::NAME_ASC:
//                    $query->orderBy('name', 'asc');
//                    break;
//                case UserSortTypes::NAME_DESC:
//                    $query->orderBy('name', 'desc');
//                    break;
//                case UserSortTypes::EMAIL_ASC:
//                    $query->orderBy('email', 'asc');
//                    break;
//                case UserSortTypes::EMAIL_DESC:
//                    $query->orderBy('email', 'desc');
//                    break;
//                default:
//                    $query->orderBy('created_at', 'desc');
//                    break;
//            }
//        }
//        dd($params);
        return $query->paginate($params['per_page'] ?? config('app.pagination'));
    }
    /**
     * @param $attributes
     * @return mixed
     */
    public function createUser(array $attributes)
    {
        if (empty($attributes['password'])) {
            $attributes['password'] = Hash::make(Str::random(16));
        }

        return DB::transaction(function () use ($attributes) {
            $user = new User($attributes);
            $user->save();

            if(!empty($attributes['role_ids'])) {
                $user->roles()->sync($attributes['role_ids']);
            }

            return $user->load('roles');
        });
    }

    /**
     * @param User $user
     * @param array $attributes
     * @return mixed
     */
    public function updateUser(User $user, array $attributes)
    {
        return DB::transaction(function () use ($attributes, $user) {
            $user->update($attributes);

//            if(!empty($attributes['role_ids'])) {
//                $user->roles()->sync($attributes['role_ids']);
//            }

            return $user;
        });
    }

//    /**
//     * @param  array  $attributes
//     */
//    public function updateUsers(array $attributes)
//    {
//        $users =  User::whereIn('id', $attributes['user_ids'])->get();
//        foreach ($users as $user) {
//            DB::transaction(function () use ($user, $attributes) {
//                $user->update($attributes);
//
//                if(!empty($attributes['role_ids'])) {
//                    $user->roles()->sync($attributes['role_ids']);
//                }
//            });
//        }
//    }

//    /**
//     * @param  array  $attributes
//     */
//    public function deleteUsers(array $attributes)
//    {
//        $users =  User::whereIn('id', $attributes['user_ids'])->get();
//        foreach ($users as $user) {
//            DB::transaction(function () use ($user) {
//                $user->roles()->sync([]);
//                $user->delete();
//            });
//        }
//    }

    /**
     * @param  User  $user
     * @return mixed
     */
    public function deleteUser(User $user)
    {
        return DB::transaction(function () use ($user) {
//            $user->roles()->sync([]);

            return $user->delete();
        });
    }

}
