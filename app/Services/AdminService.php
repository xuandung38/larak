<?php

namespace App\Services;

use App\Enums\AdminSortTypes;
use App\Models\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminService
{

    /**
     * @param $params
     * @return mixed
     */
    public function searchAdmin($params)
    {
        $query =  Admin::with('roles');
        if(!empty($params['id'])) {
            $query->where('id', $params['id']);
        }
        if(!empty($params['email'])) {
            $query->where('email', $params['email']);
        }
        if(!empty($params['phone'])) {
            $query->where('phone', $params['phone']);
        }
        if(!empty($params['search'])) {
            $query->where(function ($query) use ($params) {
                $query->where('id', $params['search'])
                    ->orWhere('email', 'like', '%' . $params['search'] . '%')
                    ->orWhere('phone', 'like', '%' . $params['search'] . '%')
                    ->orWhere('name', 'like', '%' . $params['search'] . '%');
            });
        }
        if(!empty($params['except_id'])) {
            $query->where('id', '<>', $params['except_id']);
        }
        if(!empty($params['sort_type'])) {
            switch ($params['sort_type']) {
                case AdminSortTypes::ID_ASC:
                    $query->orderBy('id', 'asc');
                    break;
                case AdminSortTypes::ID_DESC:
                    $query->orderBy('id', 'desc');
                    break;
                case AdminSortTypes::NAME_ASC:
                    $query->orderBy('name', 'asc');
                    break;
                case AdminSortTypes::NAME_DESC:
                    $query->orderBy('name', 'desc');
                    break;
                case AdminSortTypes::EMAIL_ASC:
                    $query->orderBy('email', 'asc');
                    break;
                case AdminSortTypes::EMAIL_DESC:
                    $query->orderBy('email', 'desc');
                    break;
                default:
                    $query->orderBy('created_at', 'desc');
                    break;
            }
        } else {
            $query->orderBy('id', 'desc');
        }

        $perPage = $params['per_page'] ?? config('app.pagination');
        return $query->paginate((int)$perPage);
    }
    /**
     * @param $attributes
     * @return mixed
     */
    public function createAdmin(array $attributes)
    {
        if (empty($attributes['password'])) {
            $attributes['password'] = Hash::make(Str::random(16));
        }
        return DB::transaction(function () use ($attributes) {
            $admin = new Admin($attributes);
            $admin->save();

            if(!empty($attributes['role_ids'])) {
                $admin->roles()->sync($attributes['role_ids']);
            }

            return $admin->load('roles');
        });
    }

    /**
     * @param Admin $admin
     * @param array $attributes
     * @return mixed
     */
    public function updateAdmin(Admin $admin, array $attributes)
    {
        return DB::transaction(function () use ($attributes, $admin) {
            $admin->update($attributes);

            if(!empty($attributes['role_ids'])) {
                $admin->roles()->sync($attributes['role_ids']);
            }

            return $admin->load('roles');
        });
    }

    /**
     * @param  Admin  $admin
     * @return mixed
     */
    public function deleteAdmin(Admin $admin)
    {
        return DB::transaction(function () use ($admin) {
            return $admin->delete();
        });
    }

}
