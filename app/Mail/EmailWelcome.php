<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Token;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailWelcome extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $token;

    /**
     * Create a new message instance.
     *
     * @param Token $token
     * @param User $user
     */
    public function __construct(Token $token, User $user)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome', [
            'user' => $this->user,
            'token' => $this->token,
        ])->subject('Set up your password - '.config('app.name'));
    }
}
