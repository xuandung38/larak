<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class UserSortTypes extends Enum
{
    const ID_ASC = 'id_asc';
    const ID_DESC = 'id_desc';
    const NAME_ASC = 'name_asc';
    const NAME_DESC = 'name_desc';
    const EMAIL_ASC = 'email_asc';
    const EMAIL_DESC = 'email_desc';
}
