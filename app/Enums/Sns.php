<?php


namespace App\Enums;

use MyCLabs\Enum\Enum;

class Sns extends Enum
{
	const FACEBOOK = 'facebook';
	const GOOGLE = 'google';
	const TWITTER = 'twitter';
}
