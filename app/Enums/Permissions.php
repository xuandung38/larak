<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class Permissions extends Enum
{
    const SETTING = 100;

	const VIEW_ADMIN = 200;
	const EDIT_ADMIN = 201;
	const DELETE_ADMIN = 202;

	const VIEW_USER = 300;
	const EDIT_USER = 301;
	const DELETE_USER = 302;

}
