<?php
namespace App\Enums;

use MyCLabs\Enum\Enum;

class Localization extends Enum
{
    const EN = 'en';
    const VI = 'vi';
}
