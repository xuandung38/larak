<?php

namespace App\Jobs;

use App\Enums\Queues;
use App\Enums\VerificationTypes;
use App\Mail\EmailWelcome;
use App\Models\Token;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendWelcomeMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $token;
    protected $user;
    protected $guard;

    /**
     * Create a new job instance.
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $token = new Token([
            'user_id' => $this->user->id,
            'expired_at' => date('Y-m-d H:i:s', strtotime('+1 days')),
            'token' =>  Str::random(20) . uniqid(),
            'verification_type' => VerificationTypes::RESET_PASSWORD,
        ]);
        $token->save();
        Mail::to($this->user)->send(new EmailWelcome($token, $this->user));
    }
}
