<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Token
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $admin_id
 * @property string $expired_at
 * @property string $token
 * @property int $verification_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Token newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Token newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Token query()
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereVerificationType($value)
 * @mixin \Eloquent
 */
class Token extends Model
{
    /** @var string $table */
    protected $table = 'token_verifications';

    /** @var array $fillable */
    protected $fillable = [
        'user_id',
        'admin_id',
        'order_id',
        'expired_at',
        'token',
        'verification_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


    /**
     * @return bool
     */
    public function isExpired()
    {
        $now = strtotime(now());
        $expired = strtotime($this->expired_at);
        return $now > $expired;
    }

	/**
	 * @param  mixed  $value
	 * @return Model|null
	 */
	public function resolveRouteBinding($value, $field = null)
	{
		return $this->where('token', $value)->first();
	}

}
