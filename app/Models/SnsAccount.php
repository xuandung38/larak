<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SnsAccount
 *
 * @property int $id
 * @property int $user_id
 * @property string $sns
 * @property string $sns_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount whereSns($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount whereSnsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SnsAccount whereUserId($value)
 * @mixin \Eloquent
 */
class SnsAccount extends Model
{
    /** @var string $table */
    protected $table = 'sns_accounts';

    /** @var array $fillable */
    protected $fillable = [
        'user_id',
        'sns',
        'sns_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
