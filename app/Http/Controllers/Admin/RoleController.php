<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Role\StoreRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /** @var RoleService */
    protected $roleService;

    /**
     * RoleController constructor.
     * @param  RoleService  $roleService
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return response()->json($this->roleService->searchRole($request->all()));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function composer()
    {
        return response()->json([
            'permissions' => Permission::all()
        ]);
    }

    /**
     * @param StoreRoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRoleRequest $request)
    {
        return response()->json($this->roleService->createRole($request->parameters()));
    }

    /**
     * @param Role $role
     * @param StoreRoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Role $role, StoreRoleRequest $request)
    {
        return response()->json($this->roleService->updateRole($role, $request->parameters()));
    }


    /**
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Role $role)
    {
        return response()->json($this->roleService->deleteRole($role));
    }
}
