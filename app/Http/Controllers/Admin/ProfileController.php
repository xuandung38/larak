<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Profile\ChangePasswordRequest;
use App\Http\Requests\Admin\Profile\UpdateProfileRequest;
use App\Services\AdminService;


class ProfileController extends Controller
{
    private $adminService;

    public function __construct(AdminService $userService)
    {
        $this->adminService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        return view('admin.screens.profile');
    }

    /**
     * @param UpdateProfileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(UpdateProfileRequest $request)
    {
        $this->adminService->updateAdmin(admin(), $request->parameters());

        return response()->json(['success' => true]);
    }


    /**
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(ChangePasswordRequest $request)
    {
        $this->adminService->updateAdmin(admin(), $request->parameters());

        return response()->json(['success' => true]);
    }

}
