<?php
namespace App\Http\Controllers\Admin;

use App\Enums\AdminSortTypes;
use App\Enums\Permissions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\Admin\UpdateAdminRequest;
use App\Models\Admin;
use App\Models\Role;
use App\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    /** @var AdminService */
    protected $_adminService;

    /**
     * AdminController constructor.
     * @param  AdminService  $adminService
     */
    public function __construct(AdminService $adminService)
    {
        $this->_adminService = $adminService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
    	if(Gate::denies(Permissions::VIEW_ADMIN)) {
		    abort(403);
	    }
        return response()->json($this->_adminService->searchAdmin($request->all()));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function composer()
    {
	    if(Gate::denies(Permissions::VIEW_ADMIN)) {
		    abort(403);
	    }

        $adminRoles =  Role::whereGuard('admin')->get();

        return response()->json([
            'adminRoles' => binding_collection($adminRoles),
            'sortTypes' => binding_enum(AdminSortTypes::values(), 'sort_types')
        ]);
    }

    /**
     * @param StoreAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAdminRequest $request)
    {
	    if(Gate::denies(Permissions::EDIT_ADMIN)) {
		    abort(403);
	    }
        return response()->json($this->_adminService->createAdmin($request->parameters()));
    }

    /**
     * @param Admin $admin
     * @param UpdateAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Admin $admin, UpdateAdminRequest $request)
    {
	    if(Gate::denies(Permissions::EDIT_ADMIN)) {
		    abort(403);
	    }
        return response()->json($this->_adminService->updateAdmin($admin, $request->parameters()));
    }


    /**
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Admin $admin)
    {
	    if(Gate::denies(Permissions::DELETE_ADMIN)) {
		    abort(403);
	    }
        return response()->json($this->_adminService->deleteAdmin($admin));
    }
}
