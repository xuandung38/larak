<?php
namespace App\Http\Controllers\Admin;

use App\Enums\Permissions;
use App\Enums\UserSortTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /** @var UserService */
    protected $userService;

    /**
     * UserController constructor.
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if(Gate::denies(Permissions::VIEW_USER)) {
            abort(403);
        }

        return response()->json($this->userService->searchUser($request->all()));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function composer()
    {
        if(Gate::denies(Permissions::VIEW_USER)) {
            abort(403);
        }

        return response()->json([
//            'userRoles' =>  Role::whereGuard('user')->get(),
            'sortTypes' => binding_enum(UserSortTypes::values(), 'sort_types'),
        ]);
    }

//    /**
//     * @param StoreUserRequest $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function store(StoreUserRequest $request)
//    {
//        if(Gate::denies(Permissions::EDIT_USER)) {
//            abort(403);
//        }
//
//        return response()->json($this->userService->createUser($request->parameters()));
//    }

    /**
     * @param User $user
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        if(Gate::denies(Permissions::EDIT_USER)) {
            abort(403);
        }

        return response()->json($this->userService->updateUser($user, $request->parameters()));
    }

//    /**
//     * @param  UpdateUsersRequest  $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function batchUpdate(UpdateUsersRequest $request)
//    {
//        if(Gate::denies(Permissions::EDIT_USER)) {
//            abort(403);
//        }
//
//        BatchUpdateUsers::dispatch($request->parameters());
//
//        return response()->json(['success' => true]);
//    }

//    /**
//     * @param  DeletePostsRequest  $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function batchDelete(DeletePostsRequest $request)
//    {
//        if(Gate::denies(Permissions::EDIT_USER)) {
//            abort(403);
//        }
//
//        BatchDeleteUsers::dispatch($request->parameters());
//
//        return response()->json(['success' => true]);
//    }


    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(User $user)
    {
        if(Gate::denies(Permissions::DELETE_USER)) {
            abort(403);
        }

        return response()->json($this->userService->deleteUser($user));
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
//     */
//    public function export()
//    {
//        if(Gate::denies(Permissions::VIEW_USER)) {
//            abort(403);
//        }
//
//        return Excel::download(new UsersExport(), 'users.xlsx');
//    }
}
