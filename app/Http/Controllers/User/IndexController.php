<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function index()
    {
        return redirect(route('user.dashboard'));
    }

    public function dashboard()
    {
        return view('user.screens.index');
    }

}
