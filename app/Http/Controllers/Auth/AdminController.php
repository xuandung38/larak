<?php
namespace App\Http\Controllers\Auth;

use App\Enums\Queues;
use App\Enums\VerificationTypes;
use App\Http\Requests\Auth\ForgetAdminPasswordRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Jobs\SendResetPasswordMail;
use App\Models\Admin;
use App\Models\Token;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AdminController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/admin';
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware([
            'guest:admin',
            'localization:admin'
        ])->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    public function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm(Request $request)
    {
        return view('auth.index', [
            'guard' => 'admin',
            'screen' => 'login',
            'title' => trans('dashboard.login'),
            'redirect' => $request->input('redirect', '')
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|mixed
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'success' => true,
                'user' => $this->guard()->user(),
            ]);
        }

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectTo);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect(route('auth.show_admin_login_form'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForgetPasswordForm()
    {
        return view('auth.index', [
            'guard' => 'admin',
            'screen' => 'forget',
            'title' => trans('dashboard.forget_password'),
        ]);
    }

    /**
     * @param ForgetAdminPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function createResetPasswordToken(ForgetAdminPasswordRequest $request)
    {
        try {
            $admin = Admin::whereEmail($request->input('email'))->first();
            $token = new Token([
                'admin_id' => $admin->id,
                'expired_at' => now()->addHours(3),
                'token' =>  Str::random(20) . uniqid(),
                'verification_type' => VerificationTypes::RESET_PASSWORD,
            ]);
            $token->save();
            SendResetPasswordMail::dispatch($token, $admin, 'admin')->onQueue(Queues::HIGH);

            return response()->json(['success' => true]);

        } catch (\Exception $e) {
            Log::error('Cannot create reset password token for user: '.$request->input('email'));
            Log::error($e->getMessage());
        }

        return abort(500);

    }

    /**
     * @param Token $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetPasswordForm(Token $token)
    {
        if($token->isExpired()
            || $token->verification_type !== VerificationTypes::RESET_PASSWORD
            || $token->admin_id === null
        ) {
            abort(403);
        }

        return view('auth.index', [
            'guard' => 'admin',
            'screen' => 'reset',
            'token' => $token->token,
            'title' => trans('dashboard.reset_password'),
            'redirect' => route('admin.index')
        ]);
    }

    /**
     * @param Token $token
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resetPassword(Token $token, ResetPasswordRequest $request)
    {
        if($token->isExpired()
            || $token->verification_type !== VerificationTypes::RESET_PASSWORD
            || $token->admin_id === null
        ) {
            abort(403);
        }

        $admin = $token->admin()->first();

        DB::transaction(function () use ($admin, $request, $token) {
            $admin->update($request->parameters());
            $token->delete();
        });

        $this->guard()->login($admin);

        return redirect(route('admin.index'));
    }

}
