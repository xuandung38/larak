<?php

namespace App\Http\ViewComposers\User;

use Illuminate\Contracts\View\View;

class IndexComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = user();
        $view->with([
            '_user' => $user,
            '_notifications' => []
        ]);
    }
}
