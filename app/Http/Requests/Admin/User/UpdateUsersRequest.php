<?php

namespace App\Http\Requests\Admin\User;

use App\Enums\Localization;
use App\Http\Requests\BaseRequest;
use App\Models\Role;
use Illuminate\Validation\Rule;

class UpdateUsersRequest extends BaseRequest
{
    public function rules()
    {
        $userRoles = Role::whereGuard('user')->pluck('id');
        return array_merge(parent::rules(), [
            'user_ids' => 'required|array',
            'company_id' => 'exists:companies,id',
            'lang' => 'in:' . implode(',', Localization::toArray()),
            'role_ids'  => 'array',
            'role_ids.*'  => [
                Rule::in($userRoles)
            ],
        ]);
    }

    /**
     * Prepare parameters from Form Request.
     *
     * @return array
     */
    public function parameters()
    {
        $params['user_ids'] = $this->input('user_ids');
        if(!empty($this->input('company_id'))){
            $params['company_id'] = $this->input('company_id');
        }
        if(!empty($this->input('lang'))){
            $params['lang'] = $this->input('lang');
        }
        if(!empty($this->input('role_ids'))){
            $params['role_ids'] = $this->input('role_ids');
        }

        return $params;
    }

}
