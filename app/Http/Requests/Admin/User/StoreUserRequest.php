<?php

namespace App\Http\Requests\Admin\User;

use App\Enums\Localization;
use App\Http\Requests\BaseRequest;
use App\Models\Role;
use Illuminate\Validation\Rule;

class StoreUserRequest extends BaseRequest
{
    public function rules()
    {
        $userRoles = Role::whereGuard('user')->pluck('id');

        return array_merge(parent::rules(), [
            'company_id' => 'required|exists:companies,id',
            'name'  => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users', 'email')
            ],
            'phone' => [
                'max:100',
                Rule::unique('users', 'phone')
            ],
            'role_ids'  => 'required|array',
            'role_ids.*'  => [
                'required',
                Rule::in($userRoles)
            ],
            'image' => 'max:255',
            'lang' => 'in:' . implode(',', Localization::toArray()),
        ]);
    }

    /**
     * Prepare parameters from Form Request.
     *
     * @return array
     */
    public function parameters()
    {
        return [
            'company_id'  => $this->input('company_id'),
            'name'  => $this->input('name'),
            'email' => $this->input('email'),
            'phone' => $this->input('phone'),
            'image' => $this->input('image', '/images/avatar.jpg'),
            'lang' => $this->input('lang', Localization::EN),
            'role_ids'  => $this->input('role_ids'),
        ];
    }

}
