<?php

namespace App\Http\Requests\Admin\User;

use App\Enums\Localization;
use App\Http\Requests\BaseRequest;
use App\Models\Role;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends BaseRequest
{
    public function rules()
    {
//        $userRoles = Role::whereGuard('user')->pluck('id');
        return array_merge(parent::rules(), [
            'name'  => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users', 'email')
                    ->whereNot('id', $this->input('id')),
            ],
            'phone' => [
                'nullable',
                'regex:/(0[1-9])+([0-9]{8})\b/',
                Rule::unique('users', 'phone')
                    ->whereNot('id', $this->input('id')),
            ],
//            'role_ids'  => 'required|array',
//            'role_ids.*'  => [
//                'required',
//                Rule::in($userRoles)
//            ],
//            'image' => 'max:255',
//            'lang' => 'in:' . implode(',', Localization::toArray()),
        ]);
    }

    /**
     * Prepare parameters from Form Request.
     *
     * @return array
     */
    public function parameters()
    {
        return [
            'name'  => $this->input('name'),
            'email' => $this->input('email'),
            'phone' => $this->input('phone'),
//            'image' => $this->input('image'),
//            'role_ids'  => $this->input('role_ids'),
//            'lang' => $this->input('lang', Localization::EN),
        ];
    }
}
