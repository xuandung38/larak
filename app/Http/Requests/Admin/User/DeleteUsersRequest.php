<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class DeleteUsersRequest extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'user_ids' => 'required|array',
        ]);
    }

    /**
     * Prepare parameters from Form Request.
     *
     * @return array
     */
    public function parameters()
    {
        $params['user_ids'] = $this->input('user_ids');

        return $params;
    }

}
