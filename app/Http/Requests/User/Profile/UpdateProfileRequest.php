<?php

namespace App\Http\Requests\User\Profile;

use App\Enums\Localization;
use App\Http\Requests\BaseRequest;

class UpdateProfileRequest extends BaseRequest
{

    public function rules()
    {
        $user = user();

        return array_merge(parent::rules(), [
            'name'  => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => [
                'required',
                'regex:/(0[1-9])+([0-9]{8})\b/',
                'unique:users,phone,'.$user->id,
            ],
            'lang' => 'in:' . implode(',', Localization::toArray()),
        ]);
    }

    public function parameters()
    {
        return [
            'name'  => $this->input('name'),
            'email' => $this->input('email'),
            'phone' => $this->input('phone'),
            'image' => $this->input('image'),
            'lang' => $this->input('lang'),
        ];
    }

    public function authorize()
    {
        return true;
    }
}
