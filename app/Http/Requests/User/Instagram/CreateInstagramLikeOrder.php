<?php

namespace App\Http\Requests\User\Instagram;

use App\Http\Requests\BaseRequest;

class CreateInstagramLikeOrder extends BaseRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'link'  => 'required',
            'quantity' => 'integer|min:100',
            'unit_price' => 'integer|min:50',
        ]);
    }

    /**
     * Prepare parameters from Form Request.
     *
     * @return array
     */
    public function parameters()
    {
        $params = [
            'quantity' => $this->input('quantity'),
            'unit_price' => $this->input('unit_price'),
            'note' => $this->input('note', ''),
            'user_id' => user()->id,
        ];
        $link = $this->input('link');

        if(strpos($link, 'https://www.instagram.com/p/') === false) {
            $params['post_id'] = $link;
            $params['post_link'] = 'https://www.instagram.com/p/' . $link;
        } else {
            $params['post_link'] = $link;
            $params['post_id'] = str_replace('/', '', str_replace('https://www.instagram.com/p/', '', $link));
        }

        $isWarranty = $this->input('is_warranty', false);
        $params['is_warranty'] = (boolean)$isWarranty === true;

        return $params;
    }

}
