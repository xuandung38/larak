<?php
namespace App\Http\Middleware;

use App\Models\Page;
use Closure;
use Illuminate\Http\Request;

class CrossDomain
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->has('p')) {
            abort(404);
        }

        $page = Page::find($request->input('p'));

        if(!$page->active) {
            abort(403);
        }

        $origin = $request->headers->get('origin');

        if(strpos($page->url, $origin) === false) {
            abort(403);
        }

        return $next($request)
            ->header('Access-Control-Allow-Origin', $origin)
            ->header('Access-Control-Allow-Methods', 'GET');
    }
}
