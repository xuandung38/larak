<?php
namespace App\Http\Middleware;

use App\Enums\Localization;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SetLocale
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $locale = null;

        if($guard !== null && auth($guard)->check()) {
            $locale = $guard === 'admin' ? admin()->lang : user()->lang;
        } else {
            if ($request->has('lang')) {
                $locale = $request->input('lang');
            }
        }

        if(!in_array($locale, Localization::toArray())) {
            $locale = get_ip_locale($request->ip());
        }

        App::setLocale($locale);

        return $next($request);
    }
}
