<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Admin
        view()->composer(['admin.screens.*'], 'App\Http\ViewComposers\Admin\IndexComposer');
        view()->composer(['admin.screens.admins'], 'App\Http\ViewComposers\Admin\AdminComposer');

        //User
        view()->composer(['user.screens.*'], 'App\Http\ViewComposers\User\IndexComposer');
    }
}
