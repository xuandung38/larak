<?php

use App\Enums\Localization;
use App\Models\Admin;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $systemAdminRole = Role::whereGuard('admin')->whereLevel(1)->first();
        $admin = new Admin([
            'name' => 'System Admin',
            'phone' => '0981964698',
            'email' => 'system@example.com',
            'image' => '/images/user/1.jpg',
            'lang'  => Localization::VI,
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ]);
        $admin->save();
        $admin->roles()->sync($systemAdminRole);

        $faker = Faker\Factory::create();

        for($i = 1; $i < 20; $i++) {
            $admin = new Admin([
                'name' => $faker->name,
                'phone' => $faker->unique()->phoneNumber,
                'email' => $faker->unique()->email,
                'image' => '/images/user/1.jpg',
                'lang'  => Localization::VI,
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ]);
            $admin->save();
            $admin->roles()->sync($systemAdminRole);
        }
    }
}
