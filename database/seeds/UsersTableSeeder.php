<?php

use App\Enums\Localization;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'api_key' => uniqid(md5('vietnamfb')),
            'name' => 'User',
            'phone' => '0981964698',
            'email' => 'user@example.com',
            'image' => '/images/user/1.jpg',
            'lang'  => Localization::VI,
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ]);
        $user->save();
        $role = Role::whereGuard('user')->whereLevel(1)->first();
        $user->roles()->sync($role);
    }
}
