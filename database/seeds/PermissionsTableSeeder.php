<?php

use App\Enums\Permissions;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Permissions::values() as $permission) {
            $existPermission = Permission::where('key', $permission->getValue())->first();

            if($existPermission === null) {
                $existPermission = new Permission([
                    'key' => $permission->getValue(),
                    'name' => __('enum.permissions.'.strtolower($permission->getKey())),
                ]);
            } else {
                $existPermission->name = __('enum.permissions.'.strtolower($permission->getKey()));
            }

            $existPermission->save();
        }
    }
}
