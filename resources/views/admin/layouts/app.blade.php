<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Locale -->
    <meta name="locale" content="{{ app()->getLocale() }}">
    <title>{{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="robots" content="noodp,index,follow"/>
    <meta http-equiv="X-UA-Compatible" content="requiresActiveX=true"/>
    <meta http-equiv="content-language" content="{{ app()->getLocale() }}"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>

    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/typography.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/responsive.css') }}" type="text/css">
    @yield('styles')
</head>
<body>
<div id="app">
    @yield('content')
</div>
<script src="{{ mix('js/admin_app.js') }}" defer></script>
@yield('scripts')
</body>
</html>
