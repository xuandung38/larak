import Vue from 'vue';
import Router from 'vue-router';
import AdminList from '../components/admin/AdminList'
import AdminForm from '../components/admin/AdminForm'
import UserList from '../components/admin/UserList'
import UserForm from '../components/admin/UserForm'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: AdminList
        },
        {
            path: '/admins',
            name: 'dashboard.admin',
            component: AdminList
        },
        {
            path: '/admins/:type/:id',
            name: 'dashboard.admin-edit',
            component: AdminForm
        },
        {
            path: '/admins/:type',
            name: 'dashboard.admin-create',
            component: AdminForm
        },
        {
            path: '/users',
            name: 'dashboard.user',
            component: UserList
        },
        {
            path: '/users/:type/:id',
            name: 'dashboard.user-edit',
            component: UserForm
        }
    ]
});


export default router;
