export default {
    namespaced: true,
    state: {
        notifications: [],
    },
    getters: {
        getNotifications: state => state.notifications
    },
    mutations: {
        setNotifications (state, payload) {
            state.notifications = payload;
        },
        pushNotification (state, payload) {
            state.notifications.push(payload);
        },
        removeNotification (state, payload) {
            for( let i = 0; i < state.notifications.length; i++ ) {
                if(state.notifications[i].id === payload.id) {
                    state.notifications.splice(i);
                    break;
                }
            }
        }
    }
};
