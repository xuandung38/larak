import Vue from 'vue'
import Vuex from 'vuex'
import Notification from './Notification'
import User from './User'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    Notification,
    User
  },
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  },
  strict: debug
})
