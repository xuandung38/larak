import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import VueInternationalization from 'vue-i18n';
import Locale from './i18n.js';
import route from '../../vendor/tightenco/ziggy/src/js/route';
import { Ziggy } from './ziggy';
import { Helper } from "./common/helper";
import { Request } from "./common/request";
import router from './router/user_router';
import store from './store'
import '@babel/polyfill'
import 'mutationobserver-shim'
import './plugins'

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueLodash, { lodash });
Vue.use(VueInternationalization);
Vue.prototype.Helper  = Helper;
Vue.prototype.Request  = Request;

Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    }
});

const userComponents = require.context('./components/user', true, /\.vue$/i); // eslint-disable-line no-undef
userComponents.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], userComponents(key).default));


const i18n = new VueInternationalization({
    locale: document.head.querySelector('meta[name="locale"]').content,
    messages: Locale
});

new Vue({
    store,
    el: '#app',
    i18n,
    router,
});
