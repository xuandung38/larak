import _ from 'lodash';
import { Router } from './router'

export const Editor = {
    getUrlParam(n) {
        const r = new RegExp("(?:[?&]|&)" + n + "=([^&]+)", "i"),
            o = window.location.search.match(r);
        return o && o.length > 1 ? o[1] : null
    },
    returnFileUrl(n) {
        const r = this.getUrlParam("CKEditorFuncNum");
        window.opener.CKEDITOR.tools.callFunction(r, n);
        window.close();
    },
    resetEditor() {
        if(!_.isEmpty(window.CKEDITOR)) {
            window.CKEDITOR.instances = {};
        }
    },
    initEditor(editor, data) {
        if(_.isEmpty(window.CKEDITOR)) {
            console.log('Ckeditor not init');
            return;
        }
    
        if(_.isEmpty(editor)) {
            console.log('Editor id cannot be null');
            return;
        }
        
        if(_.isEmpty(data)) {
            data = '';
        }
        const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        const locale = document.querySelector('meta[name="locale"]').getAttribute('content');
        const options = {
            removePlugins : 'easyimage, cloudservices',
            filebrowserImageBrowseUrl: Router.route('file.index'),
            filebrowserImageUploadUrl: Router.route('file.ajax_upload_file', { editor: 'ckeditor', _token: token }),
            defaultLanguage: locale,
        };
       
        if (_.isEmpty(window.CKEDITOR.instances) || _.isEmpty(window.CKEDITOR.instances[editor])) {
            window.CKEDITOR.replace(editor, options);
        } else {
            window.CKEDITOR.instances[editor].setData(data);
        }
    },
    getEditorData(editor) {
        if (!_.isEmpty(window.CKEDITOR.instances[editor])) {
            return window.CKEDITOR.instances[editor].getData();
        }
        return '';
    },

    browserImage() {
        const url = Router.route('file.index', { selector: 1 });
        const params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=1000,height=600';
        window.open(url,'Gallery', params);
    },

};


