import Vue from 'vue';
import VueRouter from 'vue-router';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import VueInternationalization from 'vue-i18n';
import Locale from './i18n.js';
import route from '../../vendor/tightenco/ziggy/src/js/route';
import { Ziggy } from './ziggy';
import { Request } from "./common/request";
import '@babel/polyfill'
import 'mutationobserver-shim'
import './plugins'
import store from './store'

Vue.use(VueRouter);
Vue.use(VueLodash, { lodash });
Vue.use(VueInternationalization);
Vue.prototype.Request  = Request;

Vue.mixin({
    methods: {
        route: (name, params, absolute) => route(name, params, absolute, Ziggy),
    }
});

const authComponents = require.context('./components/auth', true, /\.vue$/i); // eslint-disable-line no-undef
authComponents.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], authComponents(key).default));


const i18n = new VueInternationalization({
    locale: document.head.querySelector('meta[name="locale"]').content,
    messages: Locale
});

new Vue({
    store,
    el: '#app',
    i18n,
});
