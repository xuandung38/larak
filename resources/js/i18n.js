export default {
    "en": {
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, dashes and underscores.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_equals": "The {attribute} must be a date equal to {date}.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "ends_with": "The {attribute} must end with one of the following: {values}",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} are present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "starts_with": "The {attribute} must start with one of the following: {values}",
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "uuid": "The {attribute} must be a valid UUID.",
            "password_not_match": "The current password not match",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        },
        "enum": {
            "user_role": {
                "system": "System Admin",
                "super": "Super Admin",
                "normal": "User"
            },
            "sort_type": {
                "id_asc": "Oldest",
                "id_desc": "Latest",
                "latest": "Latest",
                "oldest": "Oldest",
                "name_asc": "Name a-z",
                "name_desc": "Name z-a",
                "email_asc": "Email a-z",
                "email_desc": "Email z-a"
            },
            "localization": {
                "en": "English",
                "ja": "Japanese"
            }
        },
        "passwords": {
            "password": "Passwords must be at least eight characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "message": {
            "confirm_delete_message": "Are you sure? Operation will not be able to be restored.",
            "warning_not_save_current_edit": "You have not saved editing data.",
            "enter_id_or_name_to_search": "Enter the ID or name to search",
            "login_success": "Login successfully, redirecting after 1s.",
            "password_updated": "Password updated successfully, redirecting after 1s.",
            "create_reset_password_success": "We have e-mailed your password reset link!",
            "register_success": "Successful registration, redirecting after 1s",
            "change_password_success": "Password changed successfully.",
            "update_profile_success": "Profile updated successfully.",
            "please_login_to_continue": "Please login to continue...",
            "batch_delete_success": "The system is in the process of mass deletion, please reload the page in a few minutes.",
            "batch_update_success": "The system is in the process of mass update, please reload the page in a few minutes.",
            "confirm_batch_delete": "Confirm deletion ",
            "unknown_error": "Something wrong! Please try again later!"
        },
        "dashboard": {
            "login": "Login",
            "register": "Register",
            "forget_password": "Forget password",
            "reset_password": "Reset password",
            "success": "Success",
            "error": "Error",
            "profile": "Profile",
            "change_password": "Change password",
            "logout": "Logout",
            "company": "Company",
            "user": "User",
            "administrator": "Administrator",
            "role": "Role",
            "search": "Search",
            "filter": "Filter",
            "cancel": "Cancel",
            "create": "Create",
            "edit": "Edit",
            "delete": "Delete",
            "save": "Save",
            "confirm": "Confirm",
            "per_page": "Items/page",
            "sort": "Sort by",
            "id": "ID",
            "name": "Name",
            "created_at": "Created at",
            "action": "Action",
            "email": "Email",
            "phone": "Phone",
            "balance": "Balance",
            "guard": "Guard",
            "level": "Level",
            "permission": "Permission",
            "all": "All",
            "selected": "Selected",
            "password": "Password",
            "send": "Send",
            "created": "Created",
            "admin": "Administrator",
            "update_password": "Update password",
            "confirm_delete": "Confirm to delete?",
            "current_password": "Current password",
            "new_password": "New password",
            "retype_password": "Retype password",
            "back": "Back",
            "cash": "Cash",
            "select_role": "Select role",
            "select_company": "Select company",
            "select": "Select",
            "or_login_with": "Or login with",
            "language": "Language",
            "page": "Page",
            "template_type": "Template type",
            "active": "Active",
            "activate": "Activate",
            "deactivate": "Deactivate",
            "page_setting": "Page setting",
            "basic_setting": "Basic setting",
            "embed_script": "Embed script",
            "updated": "Updated",
            "deleted": "Deleted",
            "user_object": "users",
            "remember_me": "Remember me."
        },
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds.",
            "unauthenticated": "Unauthenticated."
        },
        "datetime": {
            "sun": "Sun",
            "mon": "Mon",
            "tue": "Tue",
            "wed": "Web",
            "thu": "Thu",
            "fri": "Fri",
            "sat": "Sat",
            "sunday": "Sunday",
            "monday": "Monday",
            "tuesday": "Tuesday",
            "wednesday": "Wednesday",
            "thursday": "Thursday",
            "friday": "Friday",
            "saturday": "Saturday",
            "january": "January",
            "february": "February",
            "march": "March",
            "april": "April",
            "may": "May",
            "june": "June",
            "july": "July",
            "august": "August",
            "september": "September",
            "october": "October",
            "november": "November",
            "december": "December",
            "jan": "Jan",
            "feb": "Feb",
            "mar": "Mar",
            "apr": "Apr",
            "may2": "May",
            "jun": "Jun",
            "jul": "Jul",
            "aug": "Aug",
            "sep": "Sep",
            "oct": "Oct",
            "nov": "Nov",
            "dec": "Dec"
        }
    },
    "vi": {
        "pagination": {
            "previous": "&laquo; Trước",
            "next": "Sau &raquo;"
        },
        "validation": {
            "accepted": "Trường {attribute} phải được chấp nhận.",
            "active_url": "Trường {attribute} không phải là một URL hợp lệ.",
            "after": "Trường {attribute} phải là một ngày sau ngày {date}.",
            "after_or_equal": "Trường {attribute} phải là một ngày sau hoặc bằng ngày {date}.",
            "alpha": "Trường {attribute} chỉ có thể chứa các chữ cái.",
            "alpha_dash": "Trường {attribute} chỉ có thể chứa chữ cái, số và dấu gạch ngang.",
            "alpha_num": "Trường {attribute} chỉ có thể chứa chữ cái và số.",
            "array": "Kiểu dữ liệu của trường {attribute} phải là dạng mảng.",
            "before": "Trường {attribute} phải là một ngày trước ngày {date}.",
            "before_or_equal": "Trường {attribute} phải là một ngày trước hoặc bằng ngày {date}.",
            "between": {
                "numeric": "Trường {attribute} phải nằm trong khoảng {min} - {max}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải từ {min} - {max} kB.",
                "string": "Trường {attribute} phải từ {min} - {max} ký tự.",
                "array": "Trường {attribute} phải có từ {min} - {max} phần tử."
            },
            "boolean": "Trường {attribute} phải là true hoặc false.",
            "confirmed": "Giá trị xác nhận trong trường {attribute} không khớp.",
            "date": "Trường {attribute} không phải là định dạng của ngày-tháng.",
            "date_equals": "Trường {attribute} phải là một ngày bằng với {date}.",
            "date_format": "Trường {attribute} không giống với định dạng {format}.",
            "different": "Trường {attribute} và {other} phải khác nhau.",
            "digits": "Độ dài của trường {attribute} phải gồm {digits} chữ số.",
            "digits_between": "Độ dài của trường {attribute} phải nằm trong khoảng {min} and {max} chữ số.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "Trường {attribute} phải là một địa chỉ email hợp lệ.",
            "exists": "Giá trị đã chọn trong trường {attribute} không hợp lệ.",
            "file": "Trường {attribute} phải là một tập tin.",
            "filled": "Trường {attribute} không được bỏ trống.",
            "gt": {
                "numeric": "Trường {attribute} phải lớn hơn {max}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải lớn hơn {max} KB.",
                "string": "Trường {attribute} phải lớn hơn {max} ký tự.",
                "array": "Trường {attribute} phải lớn hơn {max} phần tử."
            },
            "gte": {
                "numeric": "Trường {attribute} phải lớn hơn hoặc bằng {max}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải lớn hơn hoặc bằng {max} KB.",
                "string": "Trường {attribute} phải lớn hơn hoặc bằng {max} ký tự.",
                "array": "Trường {attribute} phải lớn hơn hoặc bằng {max} phần tử."
            },
            "image": "Các tập tin trong trường {attribute} phải là định dạng hình ảnh.",
            "in": "Giá trị đã chọn trong trường {attribute} không hợp lệ.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "Trường {attribute} phải là một số nguyên.",
            "ip": "Trường {attribute} phải là một địa chỉa IP.",
            "ipv4": "Trường {attribute} phải là địa chỉ IPv4 hợp lệ.",
            "ipv6": "Trường {attribute} phải là địa chỉ IPv6 hợp lệ.",
            "json": "Trường {attribute} phải là chuỗi JSON hợp lệ.",
            "lt": {
                "numeric": "Trường {attribute} phải nhỏ hơn là {min}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải nhỏ hơn {min} KB.",
                "string": "Trường {attribute} phải có nhỏ hơn {min} ký tự.",
                "array": "Trường {attribute} phải có nhỏ hơn {min} phần tử."
            },
            "lte": {
                "numeric": "Trường {attribute} phải nhỏ hơn hoặc bằng là {min}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải nhỏ hơn hoặc bằng {min} KB.",
                "string": "Trường {attribute} phải có nhỏ hơn hoặc bằng {min} ký tự.",
                "array": "Trường {attribute} phải có nhỏ hơn hoặc bằng {min} phần tử."
            },
            "max": {
                "numeric": "Trường {attribute} không được lớn hơn {max}.",
                "file": "Dung lượng tập tin trong trường {attribute} không được lớn hơn {max} KB.",
                "string": "Trường {attribute} không được lớn hơn {max} ký tự.",
                "array": "Trường {attribute} không được lớn hơn {max} phần tử."
            },
            "mimes": "Trường {attribute} phải là một tập tin có định dạng: {values}.",
            "mimetypes": "Trường {attribute} phải là một tệp có định dạng là: {values}.",
            "min": {
                "numeric": "Trường {attribute} phải tối thiểu là {min}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải tối thiểu {min} KB.",
                "string": "Trường {attribute} phải có tối thiểu {min} ký tự.",
                "array": "Trường {attribute} phải có tối thiểu {min} phần tử."
            },
            "not_in": "Giá trị đã chọn trong trường {attribute} không hợp lệ.",
            "not_regex": "Trường {attribute} định dạng không hợp lệ.",
            "numeric": "Trường {attribute} phải là một số.",
            "present": "The {attribute} field must be present.",
            "regex": "Định dạng trường {attribute} không hợp lệ.",
            "required": "Trường {attribute} không được bỏ trống.",
            "required_if": "Trường {attribute} không được bỏ trống khi trường {other} là {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "Trường {attribute} không được bỏ trống khi trường {values} có giá trị.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "Trường {attribute} không được bỏ trống khi trường {values} không có giá trị.",
            "required_without_all": "Trường {attribute} không được bỏ trống khi tất cả {values} không có giá trị.",
            "same": "Trường {attribute} và {other} phải giống nhau.",
            "size": {
                "numeric": "Trường {attribute} phải bằng {size}.",
                "file": "Dung lượng tập tin trong trường {attribute} phải bằng {size} kB.",
                "string": "Trường {attribute} phải chứa {size} ký tự.",
                "array": "Trường {attribute} phải chứa {size} phần tử."
            },
            "starts_with": "Trường {attribute} phải được bắt đầu bằng một trong những giá trị sau: {values}",
            "string": "Trường {attribute} phải là một chuỗi.",
            "timezone": "Trường {attribute} phải là một múi giờ hợp lệ.",
            "unique": "Trường {attribute} đã có trong hệ thống.",
            "uploaded": "Trường {attribute} không thể tải lên.",
            "url": "Trường {attribute} không giống với định dạng một URL.",
            "uuid": "Trường {attribute} không phải là một chuỗi UUID hợp lẹ.",
            "password_not_match": "Mật khẩu hiện tại không đúng.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": {
                "name": "tên",
                "username": "tên đăng nhập",
                "email": "email",
                "first_name": "tên",
                "last_name": "họ",
                "password": "mật khẩu",
                "password_confirmation": "xác nhận mật khẩu",
                "city": "thành phố",
                "country": "quốc gia",
                "address": "địa chỉ",
                "phone": "số điện thoại",
                "mobile": "di động",
                "age": "tuổi",
                "sex": "giới tính",
                "gender": "giới tính",
                "year": "năm",
                "month": "tháng",
                "day": "ngày",
                "hour": "giờ",
                "minute": "phút",
                "second": "giây",
                "title": "tiêu đề",
                "content": "nội dung",
                "body": "nội dung",
                "description": "mô tả",
                "excerpt": "trích dẫn",
                "date": "ngày",
                "time": "thời gian",
                "subject": "tiêu đề",
                "message": "lời nhắn",
                "available": "có sẵn",
                "size": "kích thước",
                "products": "sản phẩm",
                "company_name": "tên công ty",
                "value": "giá trị",
                "limit": "giới hạn",
                "min_money_required": "số tiền yêu cầu tối thiểu",
                "max_discount_money": "số tiền giảm tối đa",
                "prod_category_id": "danh mục sản phẩm",
                "composer_id": "nghệ sỹ",
                "link_youtube": "link demo",
                "path_storage": "đường dẫn file gốc",
                "link_demo_sound": "link nhạc demo",
                "file_size": "kích thước file",
                "price": "giá",
                "expired_at": "ngày hết hạn",
                "current_password": "mật khẩu hiện tại",
                "new_password": "mật khẩu mới",
                "retype_password": "nhập lại mật khẩu",
                "amount": "số tiền",
                "display_name": "tên hiển thị",
                "bank_id": "ngân hàng"
            }
        },
        "enum": {
            "user_role": {
                "system": "System Admin",
                "super": "Super Admin",
                "normal": "User"
            },
            "sort_type": {
                "id_asc": "Cũ nhất",
                "id_desc": "Mới nhất",
                "latest": "Cũ nhất",
                "oldest": "Mới nhất",
                "name_asc": "Tên a-z",
                "name_desc": "Tên z-a",
                "email_asc": "Email a-z",
                "email_desc": "Email z-a"
            },
            "localization": {
                "en": "English",
                "ja": "Japanese",
                "vi": "Tiếng Việt"
            }
        },
        "passwords": {
            "password": "Mật khẩu phải gồm 8 ký tự và khớp với phần xác nhận.",
            "reset": "Mật khẩu đã được cập nhật!",
            "sent": "Chúng tôi đã gửi cho bạn đường dẫn thay đổi mật khẩu!",
            "token": "Mã xác nhận mật khẩu không hợp lệ.",
            "user": "Không tìm thấy thành viên với địa chỉ email này."
        },
        "message": {
            "confirm_delete_message": "Bạn chắc chắn? Thao tác này sẽ không thể khôi phục.",
            "warning_not_save_current_edit": "Bạn chưa lưu dữ liệu đang xử lý.",
            "enter_id_or_name_to_search": "Nhập tên hoặc ID để tìm kiếm",
            "login_success": "Đăng nhập thành công, chuyển hướng sau 1s.",
            "password_updated": "Đổi mật khẩu thành công, chuyển hướng sau 1s.",
            "create_reset_password_success": "Hệ thống đã gửi email thiết lập mật khẩu tới email của bạn.",
            "register_success": "Đăng ký thành công, chuyển hướng sau 1s.",
            "change_password_success": "Đổi mật khẩu thành công.",
            "update_profile_success": "Cập nhật thông tin thành công.",
            "please_login_to_continue": "Vui lòng đăng nhập để tiếp tục...",
            "batch_delete_success": "Hệ thống đang xử lý xóa hàng loạt, vui lòng tải lại trang sau ít phút.",
            "batch_update_success": "Hệ thống đang xử lý cập nhật hàng loạt, vui lòng tải lại trang sau ít phút.",
            "confirm_batch_delete": "Xác nhận xóa ",
            "unknown_error": "Hệ thống không thể thực hiện, vui lòng thử lại sau."
        },
        "dashboard": {
            "login": "Đăng nhập",
            "register": "Đăng ký",
            "forget_password": "Quên mật khẩu",
            "reset_password": "Thiết lập mật khẩu",
            "success": "Thành công",
            "error": "Lỗi",
            "profile": "Thông tin cá nhân",
            "change_password": "Đổi mật khẩu",
            "logout": "Đăng xuất",
            "company": "Công ty",
            "user": "Người dùng",
            "administrator": "Quản trị viên",
            "role": "Vai trò",
            "search": "Tìm kiếm",
            "filter": "Lọc",
            "cancel": "Hủy",
            "create": "Thêm mới",
            "edit": "Sửa",
            "delete": "Xóa",
            "save": "Lưu",
            "confirm": "Xác nhận",
            "per_page": "dòng/trang",
            "sort": "Sắp xếp",
            "id": "ID",
            "name": "Tên",
            "created_at": "Ngày tạo",
            "action": "Thao tác",
            "email": "Email",
            "phone": "Điện thoại",
            "balance": "Số đư",
            "guard": "Guard",
            "level": "Level",
            "permission": "Quyền",
            "all": "Tất cả",
            "selected": "Đã chọn",
            "password": "Mật khẩu",
            "send": "Gửi",
            "created": "Đã tạo",
            "admin": "Quản trị",
            "update_password": "Cập nhật mật khẩu",
            "confirm_delete": "Xác nhận xóa?",
            "current_password": "Mật khẩu hiện tại",
            "new_password": "Mật khẩu mới",
            "retype_password": "Nhập lại mật khẩu",
            "back": "Quay lại",
            "cash": "Số dư",
            "select_role": "Chọn vai trò",
            "select_company": "Chọn công ty",
            "select": "Chọn",
            "or_login_with": "Hoặc đăng nhập với",
            "language": "Ngôn ngữ",
            "updated": "Đã cập nhật",
            "deleted": "Đã xóa",
            "user_object": "người dùng",
            "remember_me": "Ghi nhớ đăng nhập.",
            "loading": "Đang tải",
            "select_image": "Chọn ảnh",
            "image_error": "Ảnh lỗi",
            "preview": "Xem trước",
            "facebook": "Facebook",
            "buff_like": "Tăng like",
            "buff_comment": "Tăng comment",
            "buff_follow": "Tăng theo dõi",
            "instagram": "Instagram"
        },
        "auth": {
            "failed": "Không tìm thấy thông tin đăng nhập hợp lệ.",
            "throttle": "Đăng nhập không đúng quá nhiều lần. Vui lòng thử lại sau {seconds} giây.",
            "unauthenticated": "Chưa đăng nhập."
        },
        "datetime": {
            "sun": "CN",
            "mon": "T2",
            "tue": "T3",
            "wed": "T4",
            "thu": "T5",
            "fri": "T6",
            "sat": "T7",
            "sunday": "Chủ nhật",
            "monday": "Thứ hai",
            "tuesday": "Thứ ba",
            "wednesday": "Thứ tư",
            "thursday": "Thứ năm",
            "friday": "Thứ sáu",
            "saturday": "Thứ bảy",
            "january": "Tháng Giêng",
            "february": "Tháng Hai",
            "march": "Tháng Ba",
            "april": "Tháng Tư",
            "may": "Tháng Năm",
            "june": "Tháng Sáu",
            "july": "Tháng Bảy",
            "august": "Tháng Tám",
            "september": "Tháng Chín",
            "october": "Tháng Mười",
            "november": "Tháng Mười Một",
            "december": "Tháng Chạp",
            "jan": "T01",
            "feb": "T02",
            "mar": "T03",
            "apr": "T04",
            "may2": "T05",
            "jun": "T06",
            "jul": "T07",
            "aug": "T08",
            "sep": "T09",
            "oct": "T10",
            "nov": "T11",
            "dec": "T12"
        }
    }
}
