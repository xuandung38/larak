<?php
return [
    'confirm_delete_message' => 'Are you sure? Operation will not be able to be restored.',
    'warning_not_save_current_edit' => 'You have not saved editing data.',
    'enter_id_or_name_to_search' => 'Enter the ID or name to search',
    'login_success' => 'Login successfully, redirecting after 1s.',
    'password_updated' => 'Password updated successfully, redirecting after 1s.',
    'create_reset_password_success' => 'We have e-mailed your password reset link!',
    'register_success' => 'Successful registration, redirecting after 1s',
    'change_password_success' => 'Password changed successfully.',
    'update_profile_success' => 'Profile updated successfully.',
    'please_login_to_continue' => 'Please login to continue...',
    'batch_delete_success' => 'The system is in the process of mass deletion, please reload the page in a few minutes.',
    'batch_update_success' => 'The system is in the process of mass update, please reload the page in a few minutes.',
    'confirm_batch_delete' => 'Confirm deletion ',
    'unknown_error' => 'Something wrong! Please try again later!'
];
