<?php
return [
    'login' => 'Login',
    'register' => 'Register',
    'forget_password' => 'Forget password',
    'reset_password' => 'Reset password',
    'success' => 'Success',
    'error' => 'Error',
    'profile' => 'Profile',
    'change_password' => 'Change password',
    'logout' => 'Logout',
    'company' => 'Company',
    'user' => 'User',
    'administrator' => 'Administrator',
    'role' => 'Role',
    'search' => 'Search',
    'filter' => 'Filter',
    'cancel' => 'Cancel',
    'create' => 'Create',
    'edit'  => 'Edit',
    'delete' => 'Delete',
    'save' => 'Save',
    'confirm' => 'Confirm',
    'per_page' => 'Items/page',
    'sort' => 'Sort by',
    'id' => 'ID',
    'name' => 'Name',
    'created_at' => 'Created at',
    'action' => 'Action',
    'email' => 'Email',
    'phone' => 'Phone',
    'balance' => 'Balance',
    'guard' => 'Guard',
    'level' => 'Level',
    'permission' => 'Permission',
    'all' => 'All',
    'selected' => 'Selected',
    'password' => 'Password',
    'send' => 'Send',
    'created' => 'Created',
    'admin' => 'Administrator',
    'update_password' => 'Update password',
    'confirm_delete' => 'Confirm to delete?',
    'current_password' => 'Current password',
    'new_password' => 'New password',
    'retype_password' => 'Retype password',
    'back' => 'Back',
    'cash' => 'Cash',
    'select_role' => 'Select role',
    'select_company' => 'Select company',
    'select' => 'Select',
    'or_login_with' => 'Or login with',

    'language' => 'Language',
    'page' => 'Page',
    'template_type' => 'Template type',
    'active' => 'Active',
    'activate' => 'Activate',
    'deactivate' => 'Deactivate',
    'page_setting' => 'Page setting',
    'basic_setting' => 'Basic setting',
    'embed_script' => 'Embed script',
    'updated' => 'Updated',
    'deleted' => 'Deleted',
    'user_object' => 'users',
    'remember_me' => 'Remember me.',
	'privacy' => 'privacy'
];
