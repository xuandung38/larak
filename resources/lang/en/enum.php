<?php
return [
    'user_role' => [
        'system' => 'System Admin',
        'super' => 'Super Admin',
        'normal' => 'User',
    ],
    'sort_type' => [
        'id_asc'  => 'Oldest',
        'id_desc' => 'Latest',
        'latest' => 'Latest',
        'oldest' => 'Oldest',
        'name_asc' => 'Name a-z',
        'name_desc' => 'Name z-a',
        'email_asc' => 'Email a-z',
        'email_desc' => 'Email z-a',
    ],
    'localization' => [
        'en' => 'English',
        'ja' => 'Japanese'
    ]
];
