<?php
return [
    'user_role' => [
        'system' => 'System Admin',
        'super' => 'Super Admin',
        'normal' => 'User',
    ],
    'sort_type' => [
        'id_asc'  => 'Cũ nhất',
        'id_desc' => 'Mới nhất',
        'latest' => 'Cũ nhất',
        'oldest' => 'Mới nhất',
        'name_asc' => 'Tên a-z',
        'name_desc' => 'Tên z-a',
        'email_asc' => 'Email a-z',
        'email_desc' => 'Email z-a',
    ],
    'localization' => [
        'en' => 'English',
        'ja' => 'Japanese',
        'vi' => 'Tiếng Việt'
    ],
];
