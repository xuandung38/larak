<?php
return [
    'confirm_delete_message' => 'Bạn chắc chắn? Thao tác này sẽ không thể khôi phục.',
    'warning_not_save_current_edit' => 'Bạn chưa lưu dữ liệu đang xử lý.',
    'enter_id_or_name_to_search' => 'Nhập tên hoặc ID để tìm kiếm',
    'login_success' => 'Đăng nhập thành công, chuyển hướng sau 1s.',
    'password_updated' => 'Đổi mật khẩu thành công, chuyển hướng sau 1s.',
    'create_reset_password_success' => 'Hệ thống đã gửi email thiết lập mật khẩu tới email của bạn.',
    'register_success' => 'Đăng ký thành công, chuyển hướng sau 1s.',
    'change_password_success' => 'Đổi mật khẩu thành công.',
    'update_profile_success' => 'Cập nhật thông tin thành công.',
    'please_login_to_continue' => 'Vui lòng đăng nhập để tiếp tục...',
    'batch_delete_success' => 'Hệ thống đang xử lý xóa hàng loạt, vui lòng tải lại trang sau ít phút.',
    'batch_update_success' => 'Hệ thống đang xử lý cập nhật hàng loạt, vui lòng tải lại trang sau ít phút.',
    'confirm_batch_delete' => 'Xác nhận xóa ',
    'unknown_error' => 'Hệ thống không thể thực hiện, vui lòng thử lại sau.'
];
