<?php
return [
    '1' => [
        'script' =>  'sdk/001.js',
        'css' => [
            'templates/css/01.css',
            'templates/css/common.css',
        ],
        'js' => [
            'templates/js/lib/jquery-2.2.3.min.js',
            'templates/js/lib/velocity.min.js',
            'templates/js/lib/plugins.js',
            'templates/js/app.min.js',
            'templates/js/common.js',
            'templates/js/01.js',
        ]
    ],

];
