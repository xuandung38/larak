// eslint-disable-next-line no-undef
const mix = require('laravel-mix');
// eslint-disable-next-line no-undef
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    resolve: {
        alias: {
            ziggy: path.resolve('vendor/tightenco/ziggy/dist/js/route.js'),
        },
    },
});


// if (process.env.NODE_ENV === 'development') {
//     mix.sourceMaps();
// }


mix.js('resources/js/user_app.js', 'public/js')
    .js('resources/js/admin_app.js', 'public/js')
    .js('resources/js/auth_app.js', 'public/js')
    .copy('resources/sass/error/index.scss', 'public/css/error_app.css')
    .copy('resources/sass/user/index.scss', 'public/css/user_app.css')
    .copy('resources/sass/xray/css', 'public/css')
    .copy('resources/sass/xray/fonts', 'public/fonts')
    .copy('resources/sass/xray/images', 'public/images')
    .version();
